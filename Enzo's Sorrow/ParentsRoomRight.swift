//
//  ParentsRoomRight.swift
//  Enzos Sorrow
//
//  Created by Murilo da Paixão on 05/09/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class ParentsRoomRight: MainScene {
    var sceneDoor: DoorNode!
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        if commingFromDoor != nil {
            switch (commingFromDoor?.doorType)! {
            case .Scenes:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x + 80, y: door.position.y)
                }
            default:
                break;
            }
        }
        
        if let sceneDoor = childNodeWithName("scenes") as? DoorNode {
            self.sceneDoor = sceneDoor
            self.sceneDoor.doorType = DoorType.Scenes
        }
        
        scene?.camera?.position.x = player.position.x
    }
    
    override func playerDidTouchNode(node: SKNode, at point: CGPoint) {
        let location = CGPoint(x: point.x, y: min(point.y, floor.size.height))
        
        player.walkTo(location) { 
            if node.name == "scenes" {
                let nextScene = MainScene(fileNamed: "ParentsRoomLeft")
                self.moveTo(nextScene, withDoor: self.sceneDoor)
            }
        }
    }
}