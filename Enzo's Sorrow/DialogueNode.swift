//
//  DialogueNode.swift
//  Enzos Sorrow
//
//  Created by Felipe Borges on 31/08/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

enum NodeState {
    case Questioning
    case Writing
    case Waiting
}

class DialogueNode: SKSpriteNode {
    
    var picture: SKSpriteNode?
    var dialogue: Dialogue?
    let textManager = TextManager()
    var labels = [SKLabelNode]()
    var questionLabels = [SKLabelNode]()
    var state: NodeState = .Writing
    var currentText: String?
    var alone: Bool = false
    
    convenience init(_ scene: MainScene, dialogue: Dialogue) {
    
        let frame = scene.frame
        
        let viewBottom = CGPoint(x: frame.minX, y: frame.maxY)
        let sceneBottom = scene.convertPointFromView(viewBottom)
        
        self.init(texture: SKTexture(imageNamed: "textTeste2"), color: UIColor.clearColor(), size: CGSize(width: frame.size.width, height: 150))
        self.anchorPoint = CGPoint.zero
        self.zPosition = 1
        self.position = CGPoint(x: sceneBottom.x, y: sceneBottom.y)
        self.name = "dialogue"
        textManager.allowedWidth = self.frame.size.width - 300
        self.dialogue = dialogue
        self.alone = dialogue.alone()
        loadElements()
        start()
        willWrite(false)
        
    }
    
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Init required -> Error protocol 218371236662378")
    }
    
    func start() {
        currentText = dialogue?.initialMessage()
    }
    
    func touchedNode(position: CGPoint) {
        let location = scene!.convertPoint(position, toNode: self)
        if self.children.contains(nodeAtPoint(location)) || nodeAtPoint(location) == self {
            switch state {
            case .Writing:
                textManager.stop()
                labels.forEach({ label in
                    label.text = ""
                })
                textManager.exhibitText(labels, text: currentText!, time: 0.3, completionHandler: {return})
                self.state = .Waiting
                
            case .Waiting:
                if !alone {
                self.removeAllChildren()
                let questions = dialogue?.questions()
                questionLabels.forEach({ label in
                    label.text = questions![questionLabels.indexOf(label)!]
                    addChild(label)
                })
                state = .Questioning
                } else {
                    dialogue?.currentMessage += 1
                    self.currentText = dialogue?.deliverAnswer(dialogue!.currentMessage
                    )
                    state = .Writing
                    willWrite((dialogue?.currentMessage == dialogue?.highestKey) ? true : false)
                    
                }
            
            case .Questioning:
                let questionIndex: Int = Int(location.x / (frame.width / 2))
                currentText = getAnswer((questionIndex == 1) ? dialogue!.highestKey! : dialogue!.currentMessage)
                dialogue!.currentMessage = ((dialogue!.currentMessage >= dialogue!.highestKey! - 1) ? 1 : dialogue!.currentMessage + 1)

                self.state = .Writing
                willWrite((questionIndex == 1) ? true : false)
            
            }
        }
    }
    
    private func willWrite(shallQuit: Bool) {
        self.removeAllChildren()
        addChild(picture!)
        labels.forEach { label in
            label.text = ""
            addChild(label)
        }
        
        textManager.exhibitText(labels, text: currentText!, time: 3.0, allowedWidth: 300, completionHandler: {
            self.state = .Waiting
            if shallQuit {self.removeFromParent()}
            return ()
        })
        
    }
    
    private func getAnswer(index: Int) -> String {
        return (dialogue?.deliverAnswer(index))!
    }
    
    func showQuestions() {
        self.removeAllChildren()
        let questions = dialogue?.questions()
        questionLabels.forEach { (label) in
            label.text = questions![questionLabels.indexOf(label)!]
            addChild(label)
        }
    }
    
    private func loadElements() {
        //picture
        let picture = SKSpriteNode(color: UIColor.blackColor(), size: CGSize(width: frame.size.height - 30 ,height: frame.size.height - 30))
        //picture.anchorPoint = CGPoint.zero
        picture.zPosition = 1
        picture.position = CGPoint(x: 80, y: self.size.height / 2)
        self.picture = picture
        addChild(picture)
        
        //labels
        var pos1 = CGPoint(x: picture.frame.maxX + 20 , y: frame.height * 3/4 - 10)
        var pos2 = CGPoint(x: pos1.x, y: frame.height * 1/4 + 10)
        
        let label1 = SKLabelNode(text: "") ; let label2 = SKLabelNode(text: "")
        
        label1.position = pos1 ; label2.position = pos2
        self.labels = [label1, label2]
        labels.forEach { label in
            label.zPosition = 2
            label.horizontalAlignmentMode = .Left
            label.verticalAlignmentMode = .Center
            label.fontColor = UIColor.blackColor()
            label.fontSize = 30.0
            label.name = "label\(labels.indexOf(label)! + 1)"
        }
        
        let questionLabel1 = SKLabelNode(text: "aaaa") ; let questionLabel2 = SKLabelNode(text: "bbbbbb")
        
        pos1 = CGPoint(x: frame.width * 1/4, y: frame.height / 2)
        pos2 = CGPoint(x: frame.width * 3/4, y: pos1.y)
        
        questionLabels = [questionLabel1, questionLabel2]
        questionLabel1.position = pos1 ; questionLabel2.position = pos2
        questionLabels.forEach { label in
            label.zPosition = 2
            label.horizontalAlignmentMode = .Center
            label.verticalAlignmentMode = .Center
            label.fontColor = UIColor.blackColor()
            label.fontSize = 40.0
            label.name = "questionLabel\(questionLabels.indexOf(label)! + 1)"
        }
    }
}