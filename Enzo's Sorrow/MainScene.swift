//
//  MainScene.swift
//  Enzo's Sorrow
//
//  Created by Felipe Borges on 25/08/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class MainScene: SKScene {
    
    var player: PlayerNode!
    var floor: SKSpriteNode!
    var commingFromDoor: DoorNode?
    var dialogue: Dialogue = Dialogue(withFileName: "dialogue.json")
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        if let foundPlayer = childNodeWithName("player") as? PlayerNode {
            self.player = foundPlayer
            self.player.loadTextures()
        }
        
        player.executeStoppedAnimation()
        
        let playerPhysicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: player.size.width/2, height: player.size.height/100), center: player.anchorPoint)
        
        player.physicsBody = playerPhysicsBody
        player.physicsBody?.allowsRotation = false
        player.physicsBody?.affectedByGravity = false
        player.physicsBody?.dynamic = true
        
        if let floor = childNodeWithName("floor") as? SKSpriteNode {
            self.floor = floor
        }
        
        loadConstraints()
    }
    
    func loadConstraints() {
        let leftConstraint = SKConstraint.positionX(SKRange(lowerLimit: floor.frame.minX + self.view!.frame.size.width / 2, upperLimit: floor.frame.maxX - self.view!.frame.size.width/1.9))
        
        self.camera?.constraints = [leftConstraint]
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first?.locationInNode(self) {
            let multiplierForDirection: CGFloat = (touch.x <= player.position.x) ? -1 : 1
            
            player.xScale = fabs(player.xScale) * multiplierForDirection
            
            playerDidTouchNode(nodeAtPoint(touch), at: touch)
        }
    }
    
    func playerDidTouchNode(node: SKNode, at point: CGPoint) {
        let location = CGPoint(x: point.x, y: min(point.y, floor.size.height))
        
        player.walkTo(location, completionHandler: nil)
    }
    
    func moveTo(nextScene: MainScene?, withDoor door: DoorNode) {
        nextScene?.scaleMode = .AspectFill
        nextScene?.commingFromDoor = door
        
        let transition = SKTransition.fadeWithColor(UIColor.blackColor(), duration: 1.0)
        self.view!.presentScene(nextScene!, transition: transition)
    }
}
