//
//  PlayerNode.swift
//  Enzo's Sorrow
//
//  Created by Felipe Borges on 25/08/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class PlayerNode: SKSpriteNode {
    
    let velocity: CGFloat = 170
    var walkingFrames: [SKTexture]!
    var stoppedFrames: [SKTexture]!
    
    func walkTo(location: CGPoint, completionHandler: (() -> Void)?) {
        
        let moveDuration = position.distanceTo(location) / velocity
        
        if self.actionForKey("moving") != nil {
            self.removeActionForKey("moving")
        }
        
        if self.actionForKey("moveingCamera") != nil {
            self.removeActionForKey("movingCamera")
        }
        
        if self.actionForKey("walkingInPlace") == nil {
            walking()
        }
        
        let moveAction = SKAction.moveTo(location, duration: NSTimeInterval(moveDuration))
        let moveCamera = SKAction.moveTo(CGPoint(x: location.x, y: (scene?.camera?.position.y)!), duration: NSTimeInterval(moveDuration))
        
        let doneAction = SKAction.runBlock {
            completionHandler?()
            
            self.removeAllActions()
            self.executeStoppedAnimation()
            
        }
        
        let moveActionWithDone = SKAction.sequence([moveAction, doneAction])
        
        self.runAction(moveActionWithDone, withKey: "moving")
        self.scene?.camera?.runAction(moveCamera, withKey: "movingCamera")
    }
    
    func walking() {
        self.removeActionForKey("stopped")
        self.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(walkingFrames, timePerFrame: 0.015)), withKey: "walkingInPlace")
    }
    
    func loadTextures() {
        var animatedAtlas = SKTextureAtlas(named: "walking")
        
        var numImages = animatedAtlas.textureNames.count
        
        walkingFrames = (0..<numImages).map { i in
            let textureName = String(format: "Enzo_%03d", i)
            return animatedAtlas.textureNamed(textureName)
        }
        
        animatedAtlas = SKTextureAtlas(named: "stopped")
        
        numImages = animatedAtlas.textureNames.count
        
        stoppedFrames = (0..<numImages).map { i in
            let textureName = String(format: "EnzoAnimacao_%03d", i)
            return animatedAtlas.textureNamed(textureName)
        }
    }
    
    func executeStoppedAnimation() {
        self.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(self.stoppedFrames, timePerFrame: NSTimeInterval(0.095))), withKey: "stopped")
    }
}

extension CGPoint {
    func distanceTo(point: CGPoint) -> CGFloat {
        return sqrt(pow(point.x - x, 2) + pow(point.y - y, 2))
    }
}
