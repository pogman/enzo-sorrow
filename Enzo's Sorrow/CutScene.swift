//
//  CutScene.swift
//  Enzo's Sorrow
//
//  Created by Felipe Borges on 27/08/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class CutScene: SKScene {
    
    var frames: [SKTexture]!
    var event: Int!
    let timePerFrame = 4.0
    var timer: NSTimer!
    var textTimer: NSTimer!
    var currentFrame = -1
    var currentCharacter = -1
    var destinationScene: MainScene!
    var panel: SKSpriteNode!
    var dialogues: [String] = []
    var label: SKLabelNode!
    let textManager: TextManager = TextManager()
    
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        //implement textures loading
        
        
        
        //implement dialogues loading
        
        
        
        // ---- *_* ----
        
        let panel = SKSpriteNode(texture: frames[0], size: CGSize(width: (self.view?.frame.size.width)!, height: (self.view?.frame.size.height)!))
        panel.position = camera!.position
        self.panel = panel
        
        label = childNodeWithName("label") as? SKLabelNode
        label.horizontalAlignmentMode = .Center
        addChild(self.panel)
        
        startAnimation()
    }
    
    func startAnimation() {
        timer = NSTimer(timeInterval: NSTimeInterval(timePerFrame), target: self, selector: #selector(CutScene.presentNextFrame), userInfo: nil, repeats: true)
        timer.fire()
    }
    
    func presentNextFrame() {
        if currentFrame >= frames.count - 1 {
            timer.invalidate()
            label.text = ""
            self.view?.presentScene(destinationScene, transition: SKTransition.fadeWithDuration(NSTimeInterval(1.0)))
        } else {
            currentFrame += 1
            self.panel.texture = frames[currentFrame]
            animateText()
        }
    }
    
    func animateText() {
        let duration = timePerFrame - 1
        let text = dialogues[currentFrame]
        textManager.exhibitText([self.label], text: text, time: duration, allowedWidth: 300, completionHandler: {return})
    }
    
    init(withEvent event: Int, destinationScene scene: MainScene) {
        super.init()
        self.event = event
        self.destinationScene = scene
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
}

