//
//  KitchenLeft.swift
//  Enzos Sorrow
//
//  Created by Murilo da Paixão on 04/09/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class KitchenLeft: MainScene {
    
    var door: DoorNode!
    var sceneDoor: DoorNode!
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        if commingFromDoor != nil {
            switch (commingFromDoor?.doorType)! {
            case .Door:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x, y: door.position.y + 60)
                }
            case .Scenes:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x - 80, y: door.position.y)
                    player.xScale = fabs(player.xScale) * -1
                }
            default:
                break;
            }
        }
        
        if let door = childNodeWithName("door") as? DoorNode {
            self.door = door
            self.door.doorType = DoorType.Door
        }
        
        if let sceneDoor = childNodeWithName("scenes") as? DoorNode {
            self.sceneDoor = sceneDoor
            self.sceneDoor.doorType = DoorType.Scenes
        }
        
        scene?.camera?.position.x = player.position.x
    }
    
    override func playerDidTouchNode(node: SKNode, at point: CGPoint) {
        let location = CGPoint(x: point.x, y: min(point.y, floor.size.height))
        
        player.walkTo(location) {
            if node.name == "door" {
                let nextScene = MainScene(fileNamed: "FireplaceRoomRight")
                self.moveTo(nextScene, withDoor: self.door)
            }
            
            if node.name == "scenes" {
                let nextScene = MainScene(fileNamed: "KitchenRight")
                self.moveTo(nextScene, withDoor: self.sceneDoor)
            }
        }
    }
}