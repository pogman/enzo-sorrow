//
//  LivingRoomRight.swift
//  Enzos Sorrow
//
//  Created by Murilo da Paixão on 02/09/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class LivingRoomRight: MainScene {
    
    var stairs: DoorNode!
    var sceneDoorLeft: DoorNode!
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        if commingFromDoor != nil {
            switch (commingFromDoor?.doorType)! {
            case .Stairs:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x - 80, y: door.position.y)
                    player.xScale = fabs(player.xScale) * -1
                }
            case .Scenes:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x + 80, y: door.position.y)
                }
            default:
                break;
            }
        }
        
        if let stairs = childNodeWithName("stairs") as? DoorNode {
            self.stairs = stairs
            self.stairs.doorType = DoorType.Stairs
        }
        
        if let sceneDoorLeft = childNodeWithName("scenes1") as? DoorNode {
            self.sceneDoorLeft = sceneDoorLeft
            self.sceneDoorLeft.doorType = DoorType.Scenes
        }
        
        scene?.camera?.position.x = player.position.x
    }
    
    override func playerDidTouchNode(node: SKNode, at point: CGPoint) {
        let location = CGPoint(x: point.x, y: min(point.y, floor.size.height))
        
        player.walkTo(location) { 
            if node.name == "stairs" {
                let nextScene = MainScene(fileNamed: "UpHallLeft")
                self.moveTo(nextScene, withDoor: self.stairs)
            }
            
            if node.name == "scenes1" {
                let nextScene = MainScene(fileNamed: "LivingRoomMiddle")
                self.moveTo(nextScene, withDoor: self.sceneDoorLeft)
            }
        }
    }
}