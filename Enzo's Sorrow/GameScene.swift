//
//  GameScene.swift
//  Enzo's Sorrow
//
//  Created by Felipe Borges on 25/08/16.
//  Copyright (c) 2016 Felipe Borges. All rights reserved.
//

import SpriteKit

class GameScene: MainScene {
    
    var scenesDoor: DoorNode!
    var dialogueNode: DialogueNode!
   
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        if commingFromDoor != nil {
            switch (commingFromDoor?.doorType)! {
            case .Scenes:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x - 80, y: door.position.y)
                    player.xScale = fabs(player.xScale) * -1
                }
            default:
                break;
            }
        }
        
        
        dialogueNode = DialogueNode(self, dialogue: dialogue)
        dialogueNode.zPosition = 5
        
        if let scenesDoor = childNodeWithName("scenes") as? DoorNode {
            self.scenesDoor = scenesDoor
            self.scenesDoor.doorType = DoorType.Scenes
        }
        
        camera?.position.x = player.position.x
    }
    
    override func playerDidTouchNode(node: SKNode, at point: CGPoint) {
        let location = CGPoint(x: point.x, y: min(point.y, floor.size.height))
        
        if nodesAtPoint(point).contains(self.dialogueNode) {
            dialogueNode.touchedNode(point)
        } else if !(scene?.children.contains(self.dialogueNode))! {
            player.walkTo(location) {
                if node.name == "scenes" {
                    let nextScene = MainScene(fileNamed: "Room2")
                    self.moveTo(nextScene, withDoor: self.scenesDoor)
                }
                
                if node.name == "mother" {
                    self.addChild(self.dialogueNode)
                }
            }
        }
    }
}
