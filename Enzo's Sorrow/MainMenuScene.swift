//
//  MenuScene.swift
//  Enzo's Sorrow
//
//  Created by Felipe Borges on 27/08/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class MainMenuScene: SKScene {
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        let menuSong = SKAudioNode(fileNamed: "menu_2.mp3")
        self.addChild(menuSong)
        
        menuSong.runAction(SKAction.play())
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        let location = touches.first!.locationInNode(self)
        let touchedNode = nodeAtPoint(location)
        
        if touchedNode.name! == "playButton" {
            
            let mainScene = MainScene(fileNamed: "GameScene")
            mainScene!.scaleMode = .AspectFill
            mainScene?.commingFromDoor = nil
            
            let transition = SKTransition.fadeWithColor(UIColor.blackColor(), duration: 1.0)
            self.view!.presentScene(mainScene!, transition: transition)
        }
    }
}