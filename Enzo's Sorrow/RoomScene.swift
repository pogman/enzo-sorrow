//
//  RoomScene.swift
//  Enzo's Sorrow
//
//  Created by Felipe Borges on 29/08/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class RoomScene: MainScene {
    
    var scenesDoor: DoorNode!
    var normalDoor: DoorNode!
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        if commingFromDoor != nil {
            switch (commingFromDoor?.doorType)! {
            case .Scenes:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x + 80, y: door.position.y)
                }
            case .Door:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x, y: door.position.y + 40)
                    player.xScale = fabs(player.xScale) * -1
                }
            default:
                break;
            }
        }
        
        if let scenesDoor = childNodeWithName("scenes") as? DoorNode {
            self.scenesDoor = scenesDoor
            self.scenesDoor.doorType = DoorType.Scenes
        }
        
        if let normalDoor = childNodeWithName("door") as? DoorNode {
            self.normalDoor = normalDoor
            self.normalDoor.doorType = DoorType.Door
        }
        
        camera?.position.x = player.position.x
    }
    
    override func playerDidTouchNode(node: SKNode, at point: CGPoint) {
        let location = CGPoint(x: point.x, y: min(point.y, floor.size.height))
        
        player.walkTo(location) {
            if node.name == "scenes" {
                let nextScene = MainScene(fileNamed: "GameScene")
                self.moveTo(nextScene, withDoor: self.scenesDoor)
            }
            
            if node.name == "door" {
                let nextScene = MainScene(fileNamed: "UpHallMiddle")
                self.moveTo(nextScene, withDoor: self.normalDoor)
            }
        }
    }
}