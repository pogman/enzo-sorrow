//
//  TextManager.swift
//  Enzo's Sorrow
//
//  Created by Felipe Borges on 29/08/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class TextManager: NSObject {
    var text: String?
    var textTimer: NSTimer?
    var currentCharacter: Int?
    var labels : [SKLabelNode] = [SKLabelNode]()
    var currentLabel = 0
    var allowedWidth: CGFloat?
    var completionHandler: ()->()? = {return}
    
    func exhibitText(labels: [SKLabelNode], text: String, time: Double, allowedWidth: CGFloat? = nil, completionHandler: ()->()?) {
        currentCharacter = -1
        currentLabel = 0
        self.completionHandler = completionHandler
        self.text = text
        self.labels = labels
        let timePerCharacter = time / Double(text.characters.count)
        textTimer = NSTimer.scheduledTimerWithTimeInterval(NSTimeInterval(timePerCharacter), target: self, selector: #selector(TextManager.nextCharacter), userInfo: nil, repeats: true)
        
        
    }
    
    

    func nextCharacter() {
        if currentCharacter >= text!.characters.count - 1 {
            stop()
            completionHandler()
            currentCharacter = -1
        } else {
            currentCharacter = currentCharacter! + 1
            labels[currentLabel].text?.append(self.text![currentCharacter!])
        }
        
        if labels[currentLabel].frame.width >= allowedWidth && labels.count >= currentLabel + 1 && labels[currentLabel].text?.characters.last == " " {
            currentLabel += 1
        }
    }
    
    func stop() {
        
        textTimer?.invalidate()
        
    }
    
}

extension String {
    
    subscript (i: Int) -> Character {
        return self[self.startIndex.advancedBy(i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = startIndex.advancedBy(r.startIndex)
        let end = start.advancedBy(r.endIndex - r.startIndex)
        return self[Range(start ..< end)]
        
    }
    
    
}
