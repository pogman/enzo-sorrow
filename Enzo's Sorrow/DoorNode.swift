//
//  DoorNode.swift
//  Enzo's Sorrow
//
//  Created by Felipe Borges on 25/08/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

enum DoorType {
    case Door
    case Stairs
    case Scenes
}

class DoorNode: SKSpriteNode {
    var doorType: DoorType!
    
    func moveTo(destination: MainScene) {
        let transition = SKTransition.fadeWithDuration(NSTimeInterval(1.0))
        
        self.scene!.view?.presentScene(destination, transition: transition)
    }
}