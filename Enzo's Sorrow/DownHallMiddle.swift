//
//  DownHallMiddle.swift
//  Enzos Sorrow
//
//  Created by Murilo da Paixão on 04/09/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class DownHallMiddle: MainScene {
    var sceneRight: DoorNode!
    var sceneLeft: DoorNode!
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        if commingFromDoor != nil {
            switch (commingFromDoor?.doorType)! {
            case .Scenes:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    if door.name == "scenes1" {
                        player.position = CGPoint(x: door.position.x - 80, y: door.position.y)
                        player.xScale = fabs(player.xScale) * -1
                    } else {
                        player.position = CGPoint(x: door.position.x + 80, y: door.position.y)
                    }
                }
            default:
                break;
            }
        }
        
        if let sceneRight = childNodeWithName("scenes1") as? DoorNode {
            self.sceneRight = sceneRight
            self.sceneRight.doorType = DoorType.Scenes
        }
        
        if let sceneLeft = childNodeWithName("scenes2") as? DoorNode {
            self.sceneLeft = sceneLeft
            self.sceneLeft.doorType = DoorType.Scenes
        }
        
        scene?.camera?.position.x = player.position.x
    }
    
    override func playerDidTouchNode(node: SKNode, at point: CGPoint) {
        let location = CGPoint(x: point.x, y: min(point.y, floor.size.height))
        
        player.walkTo(location) {
            if node.name == "scenes1" {
                let nextScene = MainScene(fileNamed: "DownHallRight")
                self.moveTo(nextScene, withDoor: self.sceneRight)
            }
            
            if node.name == "scenes2" {
                let nextScene = MainScene(fileNamed: "DownHallLeft")
                self.moveTo(nextScene, withDoor: self.sceneLeft)
            }
        }
    }

}
