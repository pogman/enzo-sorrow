//
//  UpHallLeft.swift
//  Enzos Sorrow
//
//  Created by Murilo da Paixão on 01/09/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class UpHallLeft: MainScene {
    
    var scenesDoor: DoorNode!
    var stairs: DoorNode!
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        if commingFromDoor != nil {
            switch (commingFromDoor?.doorType)! {
            case .Scenes:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x - 80, y: door.position.y)
                    player.xScale = fabs(player.xScale) * -1
                }
            case .Stairs:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x, y: door.position.y + 80)
                }
            default:
                break;
            }
        }
        
        if let scenesDoor = childNodeWithName("scenes1") as? DoorNode {
            self.scenesDoor = scenesDoor
            self.scenesDoor.doorType = DoorType.Scenes
        }
        
        if let stairs = childNodeWithName("stairs") as? DoorNode {
            self.stairs = stairs
            self.stairs.doorType = DoorType.Stairs
        }
        
        scene?.camera?.position.x = player.position.x
    }
    
    override func playerDidTouchNode(node: SKNode, at point: CGPoint) {
        let location = CGPoint(x: point.x, y: min(point.y, floor.size.height))
        
        player.walkTo(location) { 
            if node.name == "scenes1" {
                let nextScene = MainScene(fileNamed: "UpHallMiddle")
                self.moveTo(nextScene, withDoor: self.scenesDoor)
            }
            
            if node.name == "stairs" {
                let nextScene = MainScene(fileNamed: "LivingRoomRight")
                self.moveTo(nextScene, withDoor: self.stairs)
            }
        }
    }
}
