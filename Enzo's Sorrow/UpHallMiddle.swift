//
//  UpHall.swift
//  Enzos Sorrow
//
//  Created by Murilo da Paixão on 01/09/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import SpriteKit

class UpHallMiddle: MainScene {
    
    var normalDoor: DoorNode!
    var sceneRight: DoorNode!
    var sceneLeft:  DoorNode!
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        if commingFromDoor != nil {
            switch (commingFromDoor?.doorType)! {
            case .Door:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x, y: door.position.y - 80)
                    player.xScale = fabs(player.xScale) * -1
                }
            case .Scenes:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    if door.name == "scenes2" {
                        player.position = CGPoint(x: door.position.x - 80, y: door.position.y)
                        player.xScale = fabs(player.xScale) * -1
                    } else {
                        player.position = CGPoint(x: door.position.x + 80, y: door.position.y)
                    }
                }
            default:
                break;
            }
        }
        
        if let normalDoor = childNodeWithName("door") as? DoorNode {
            self.normalDoor = normalDoor
            self.normalDoor.doorType = DoorType.Door
        }
        
        if let sceneRight = childNodeWithName("scenes2") as? DoorNode {
            self.sceneRight = sceneRight
            self.sceneRight.doorType = DoorType.Scenes
        }
        
        if let sceneLeft = childNodeWithName("scenes1") as? DoorNode {
            self.sceneLeft = sceneLeft
            self.sceneLeft.doorType = DoorType.Scenes
        }
        
        camera?.position.x = player.position.x
    }
    
    override func playerDidTouchNode(node: SKNode, at point: CGPoint) {
        let location = CGPoint(x: point.x, y: min(point.y, floor.size.height))
        
        player.walkTo(location) {
            if node.name == "door" {
                let nextScene = MainScene(fileNamed: "Room2")
                self.moveTo(nextScene, withDoor: self.normalDoor)
            }
            
            if node.name == "scenes2" {
                let nextScene = MainScene(fileNamed: "UpHallRight")
                self.moveTo(nextScene, withDoor: self.sceneRight)
            }
            
            if node.name == "scenes1" {
                let nextScene = MainScene(fileNamed: "UpHallLeft")
                self.moveTo(nextScene, withDoor: self.sceneLeft)
            }
        }
    }
}
