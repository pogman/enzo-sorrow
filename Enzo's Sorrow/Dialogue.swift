//
//  Dialogue.swift
//  Enzos Sorrow
//
//  Created by Felipe Borges on 29/08/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SwiftyJSON

class Dialogue: NSObject {
    private var json: JSON?
    var currentMessage: Int = 1
    var highestKey: Int?
    
    init(withFileName fileName: String) {
        super.init()
        let path = NSBundle.mainBundle().resourcePath?.stringByAppendingString("/\(fileName)")
        let data = NSData(contentsOfFile: path!)
        let json = JSON(data: data!)
        
        self.json = json
        
        highestKey = (self.json?["enzo"].dictionaryObject?.keys.count)! - 1
      
        
    }
    
    func deliverAnswer(message: Int) -> String{
        return self.json!["talker"]["\(message)"].string!
    }
    
    func initialMessage() -> String{
        return self.json!["talker"]["0"].string!
    }
    
    func questions(index: Int? = nil) -> [String] {
        
        let question1 = self.json?["enzo"]["\(((index != nil) ? index! : self.currentMessage))"].string!
        let question2 = self.json?["enzo"]["\(self.highestKey!)"].string!
        
        
        return [question1!, question2!]
    }
    
    func alone() -> Bool {
        return self.json!["enzo"].isEmpty
    }
    
    
    
    
}