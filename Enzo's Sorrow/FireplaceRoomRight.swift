//
//  FireplaceRoomRight.swift
//  Enzos Sorrow
//
//  Created by Murilo da Paixão on 04/09/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class FireplaceRoomRight: MainScene {
    var sceneDoor: DoorNode!
    var door: DoorNode!
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        if commingFromDoor != nil {
            switch (commingFromDoor?.doorType)! {
            case .Scenes:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x + 80, y: door.position.y)
                }
            case .Door:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x - 80, y: door.position.y)
                    player.xScale = fabs(player.xScale) * -1
                }
            default:
                break;
            }
        }
        
        if let sceneDoor = childNodeWithName("scenes2") as? DoorNode {
            self.sceneDoor = sceneDoor
            self.sceneDoor.doorType = DoorType.Scenes
        }
        
        if let door = childNodeWithName("door") as? DoorNode {
            self.door = door
            self.door.doorType = DoorType.Door
        }
        
        scene?.camera?.position.x = player.position.x
    }
    
    override func playerDidTouchNode(node: SKNode, at point: CGPoint) {
        let location = CGPoint(x: point.x, y: min(point.y, floor.size.height))
        
        player.walkTo(location) { 
            if node.name == "scenes2" {
                let nextScene = MainScene(fileNamed: "FireplaceRoomMiddle")
                self.moveTo(nextScene, withDoor: self.sceneDoor)
            }
            
            if node.name == "door" {
                let nextScene = MainScene(fileNamed: "KitchenLeft")
                self.moveTo(nextScene, withDoor: self.door)
            }
        }
    }
}
