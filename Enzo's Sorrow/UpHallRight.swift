//
//  UpHallRight.swift
//  Enzos Sorrow
//
//  Created by Murilo da Paixão on 01/09/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class UpHallRight: MainScene {
    
    var scenesDoor: DoorNode!
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        if commingFromDoor != nil {
            switch (commingFromDoor?.doorType)! {
            case .Scenes:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x + 80, y: door.position.y)
                }
            default:
                break;
            }
        }
        
        if let scenesDoor = childNodeWithName("scenes2") as? DoorNode {
            self.scenesDoor = scenesDoor
            self.scenesDoor.doorType = DoorType.Scenes
        }
        
        scene?.camera?.position.x = player.position.x
    }
    
    override func playerDidTouchNode(node: SKNode, at point: CGPoint) {
        let location = CGPoint(x: point.x, y: min(point.y, floor.size.height))
        
        player.walkTo(location) {
            if node.name == "scenes2" {
                let nextScene = MainScene(fileNamed: "UpHallMiddle")
                self.moveTo(nextScene, withDoor: self.scenesDoor)
            }
        }
    }
}
