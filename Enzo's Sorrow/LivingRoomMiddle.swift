//
//  LivingRoomMiddle.swift
//  Enzos Sorrow
//
//  Created by Murilo da Paixão on 02/09/16.
//  Copyright © 2016 Felipe Borges. All rights reserved.
//

import Foundation
import SpriteKit

class LivingRoomMiddle: MainScene {
    
    var sceneRightDoor: DoorNode!
    var sceneLeftDoor: DoorNode!
    var door: DoorNode!
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
     
        if commingFromDoor != nil {
            switch (commingFromDoor?.doorType)! {
            case .Scenes:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    if door.name == "scenes1" {
                        player.position = CGPoint(x: door.position.x - 80, y: door.position.y)
                        player.xScale = fabs(player.xScale) * -1
                    } else {
                        player.position = CGPoint(x: door.position.x + 80, y: door.position.y)
                    }
                }
            case .Door:
                if let door = childNodeWithName((commingFromDoor?.name)!) {
                    player.position = CGPoint(x: door.position.x, y: door.position.y - 160)
                    player.xScale = fabs(player.xScale) * -1
                }
            default:
                break;
            }
        }
        
        if let sceneRightDoor = childNodeWithName("scenes1") as? DoorNode {
            self.sceneRightDoor = sceneRightDoor
            self.sceneRightDoor.doorType = DoorType.Scenes
        }
        
        if let sceneLeftDoor = childNodeWithName("scenes2") as? DoorNode {
            self.sceneLeftDoor = sceneLeftDoor
            self.sceneLeftDoor.doorType = DoorType.Scenes
        }
        
        if let door = childNodeWithName("door") as? DoorNode {
            self.door = door
            self.door.doorType = DoorType.Door
        }
        
        scene?.camera?.position.x = player.position.x
    }
    
    override func playerDidTouchNode(node: SKNode, at point: CGPoint) {
        let location = CGPoint(x: point.x, y: min(point.y, floor.size.height))
        
        player.walkTo(location) { 
            if node.name == "scenes1" {
                let nextScene = MainScene(fileNamed: "LivingRoomRight")
                self.moveTo(nextScene, withDoor: self.sceneRightDoor)
            }
            
            if node.name == "scenes2" {
                let nextScene = MainScene(fileNamed: "LivingRoomLeft")
                self.moveTo(nextScene, withDoor: self.sceneLeftDoor)
            }
            
            if node.name == "door" {
                let nextScene = MainScene(fileNamed: "FireplaceRoomMiddle")
                self.moveTo(nextScene, withDoor: self.door)
            }
        }
    }
}
